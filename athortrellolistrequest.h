#ifndef ATHORTRELLOLISTREQUEST_H
#define ATHORTRELLOLISTREQUEST_H

#include <athor-trello-api_global.h>

#include "athortrellorequest.h"

ATHOR_TRELLO_API_BEGIN_NAMESPACE

class AthorTrelloCardModel;

class AthorTrelloListRequest : public AthorTrelloRequest {
    using AthorTrelloRequest::AthorTrelloRequest;
public:
    QList<AthorTrelloCardModel*> findCardByIdList( const QString& id );

};

ATHOR_TRELLO_API_END_NAMESPACE

#endif // ATHORTRELLOLISTREQUEST_H

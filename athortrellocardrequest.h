#ifndef ATHORTRELLOCARDREQUEST_H
#define ATHORTRELLOCARDREQUEST_H

#include <athor-trello-api_global.h>

#include "athortrellorequest.h"

ATHOR_TRELLO_API_BEGIN_NAMESPACE

class AthorTrelloCardModel;

class AthorTrelloCardRequest : public AthorTrelloRequest {
    using AthorTrelloRequest::AthorTrelloRequest;
public:
    void insert( const QString& idList, const QString& name );

};

ATHOR_TRELLO_API_END_NAMESPACE

#endif // ATHORTRELLOCARDREQUEST_H

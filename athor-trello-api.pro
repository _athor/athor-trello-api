#---------------------------------------------------
# Project created by kielsonzinn 2020-03-31T19:24:05
#---------------------------------------------------

QT -= gui
QT += network

TEMPLATE = lib
DEFINES += ATHORTRELLOAPI_LIBRARY

CONFIG(debug, debug|release) {
    DESTDIR = build/debug
}
CONFIG(release, debug|release) {
    DESTDIR = build/release
}

OBJECTS_DIR = $$DESTDIR/.obj
MOC_DIR = $$DESTDIR/.moc
RCC_DIR = $$DESTDIR/.qrc
UI_DIR = $$DESTDIR/.u

SOURCES += \
    athortrelloboardrequest.cpp \
    athortrellocardrequest.cpp \
    athortrellolistrequest.cpp \
    athortrellorequest.cpp \
    data/athortrellocardmodel.cpp \
    data/athortrellolistmodel.cpp

HEADERS += \
    athor-trello-api_global.h \
    athortrelloboardrequest.h \
    athortrellocardrequest.h \
    athortrellolistrequest.h \
    athortrellorequest.h \
    data/athortrellocardmodel.h \
    data/athortrellolistmodel.h

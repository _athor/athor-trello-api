#ifndef ATHORTRELLOREQUEST_H
#define ATHORTRELLOREQUEST_H

#include <functional>

#include <QUrl>
#include <QNetworkRequest>

#include <athor-trello-api_global.h>

class QJsonDocument;
class QNetworkReply;
class QNetworkAccessManager;

ATHOR_TRELLO_API_BEGIN_NAMESPACE

class AthorTrelloRequest : public QNetworkRequest {
    using QNetworkRequest::QNetworkRequest;
public:
    AthorTrelloRequest();
    AthorTrelloRequest( const QString& url, const QString& key, const QString& token );

    QJsonDocument get();
    QJsonDocument put( const QByteArray& data );
    QJsonDocument post( const QByteArray& data );

    int timeout() const;
    void setTimeout( int timeout );

    void setUrl( const QString& url );

protected:
    QUrl _url;

    QJsonDocument send( const std::function<QNetworkReply*( QNetworkAccessManager& acessManager )>& exec );

private:
    QString _key = "";
    QString _token = "";
    int _timeout = 10000;

};

ATHOR_TRELLO_API_END_NAMESPACE

#endif // ATHORTRELLOREQUEST_H

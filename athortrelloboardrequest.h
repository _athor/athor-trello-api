#ifndef ATHORTRELLOBOARDREQUEST_H
#define ATHORTRELLOBOARDREQUEST_H

#include <athor-trello-api_global.h>

#include "athortrellorequest.h"

ATHOR_TRELLO_API_BEGIN_NAMESPACE

class AthorTrelloListModel;

class AthorTrelloBoardRequest : public AthorTrelloRequest {
    using AthorTrelloRequest::AthorTrelloRequest;
public:
    QList<AthorTrelloListModel*> findListByIdBoard( const QString& id );

};

ATHOR_TRELLO_API_END_NAMESPACE

#endif // ATHORTRELLOBOARDREQUEST_H

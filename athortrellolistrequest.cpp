#include "athortrellolistrequest.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

#include "data/athortrellocardmodel.h"

ATHOR_TRELLO_API_BEGIN_NAMESPACE

QList<AthorTrelloCardModel*> AthorTrelloListRequest::findCardByIdList( const QString& id ) {

    QString url = _url.url();
    url = url.append( "/1/lists/" );
    url = url.append( id );
    url = url.append( "/cards" );

    _url = QUrl( url );
    QJsonArray json = AthorTrelloRequest::get().array();

    QList<AthorTrelloCardModel*> responseList = {};

    for ( QJsonValue response : json ) {
        AthorTrelloCardModel* athorTrelloListModel = new AthorTrelloCardModel();
        athorTrelloListModel->fromJson( response.toObject() );

        responseList.append( athorTrelloListModel );
    }

    return responseList;

}

ATHOR_TRELLO_API_END_NAMESPACE

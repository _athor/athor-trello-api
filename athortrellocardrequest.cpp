#include "athortrellocardrequest.h"

#include <QJsonObject>
#include <QJsonDocument>

ATHOR_TRELLO_API_BEGIN_NAMESPACE

void AthorTrelloCardRequest::insert( const QString& idList, const QString& name ) {

    QString url = _url.url();
    url = url.append( "/1/cards" );

    QJsonObject json;
    json["name"] = name;
    json["idList"] = idList;

    _url = QUrl( url );

    AthorTrelloRequest::setRawHeader( "Content-Type", "application/json; charset=utf-8" );
    AthorTrelloRequest::post( QJsonDocument( json ).toJson() );

}

ATHOR_TRELLO_API_END_NAMESPACE

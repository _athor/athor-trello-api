#include "athortrellorequest.h"

#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>

ATHOR_TRELLO_API_BEGIN_NAMESPACE

AthorTrelloRequest::AthorTrelloRequest() = default;

AthorTrelloRequest::AthorTrelloRequest( const QString& url, const QString& key, const QString& token ) :
    _url( url ),
    _key( key ),
    _token( token ) {}

QJsonDocument AthorTrelloRequest::get() {
    return send([this]( QNetworkAccessManager& acessManager ) {
        return acessManager.get( *this );
    } );
}

QJsonDocument AthorTrelloRequest::put( const QByteArray& data ) {
    return send([&]( QNetworkAccessManager& acessManager ) {
        return acessManager.put( *this, data );
    } );
}

QJsonDocument AthorTrelloRequest::post( const QByteArray& data ) {
    return send([&]( QNetworkAccessManager& acessManager ) {
        return acessManager.post( *this, data );
    } );
}

int AthorTrelloRequest::timeout() const {
    return _timeout;
}

void AthorTrelloRequest::setTimeout( int timeout ) {
    _timeout = timeout;
}

void AthorTrelloRequest::setUrl( const QString& url ) {
    _url = QUrl( url );
}

QJsonDocument AthorTrelloRequest::send( const std::function<QNetworkReply*(QNetworkAccessManager&)>& exec ) {

    qDebug() << "AthorTrelloRequest::send [URL]" << _url;

    QJsonDocument json;
    QNetworkAccessManager accessManager;

    QString url = _url.toString();
    url = url.append( "?key=" );
    url = url.append( _key );
    url = url.append( "&token=" );
    url = url.append( _token );

    _url = url;
    QNetworkRequest::setUrl( _url );

    QNetworkReply* reply = exec( accessManager );

    QTimer singleShot;
    singleShot.setSingleShot( true );

    QEventLoop eventLoop;
    QObject::connect( &singleShot, &QTimer::timeout, &eventLoop, &QEventLoop::quit );
    QObject::connect( &accessManager, &QNetworkAccessManager::finished, &eventLoop, &QEventLoop::quit );
    singleShot.start( _timeout );
    eventLoop.exec();

    if ( singleShot.isActive() ) {
        qDebug() << "AthorTrelloRequest::send finished";
        singleShot.stop();

        if ( reply->error() == QNetworkReply::NoError ) {
            qDebug() << "AthorTrelloRequest::send ok";
            json = QJsonDocument::fromJson( reply->readAll() );
            delete reply;

        } else {
            QString error = reply->errorString();
            qCritical() << "AthorTrelloRequest::send [ERROR]" << error;
            delete reply;

            throw std::runtime_error( error.toStdString().c_str() );
        }

    } else {
        qCritical() << "AthorTrelloRequest::send timeout";
        QObject::disconnect( &singleShot, &QTimer::timeout, &eventLoop, &QEventLoop::quit );

        reply->abort();
        QString error = reply->errorString();
        delete reply;

        throw std::runtime_error( error.toStdString().c_str() );
    }

    return json;
}

ATHOR_TRELLO_API_END_NAMESPACE

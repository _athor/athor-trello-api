#ifndef ATHORTRELLOCARDMODEL_H
#define ATHORTRELLOCARDMODEL_H

#include <QString>

#include <athor-trello-api_global.h>

class QJsonObject;

ATHOR_TRELLO_API_BEGIN_NAMESPACE

class AthorTrelloCardModel {

public:
    AthorTrelloCardModel();

    QString id() const;
    void setId( const QString& id );

    QString name() const;
    void setName( const QString& name );

    void fromJson( const QJsonObject& json );

private:
    QString _id = "";
    QString _name = "";

};

ATHOR_TRELLO_API_END_NAMESPACE

#endif // ATHORTRELLOCARDMODEL_H

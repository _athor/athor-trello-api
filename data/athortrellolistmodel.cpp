#include "athortrellolistmodel.h"

#include <QJsonObject>

ATHOR_TRELLO_API_BEGIN_NAMESPACE

AthorTrelloListModel::AthorTrelloListModel() {}

QString AthorTrelloListModel::id() const {
    return _id;
}

void AthorTrelloListModel::setId( const QString& id ) {
    _id = id;
}

QString AthorTrelloListModel::name() const {
    return _name;
}

void AthorTrelloListModel::setName( const QString& name ) {
    _name = name;
}

void AthorTrelloListModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toString();
    _name = json["name"].toString();
}

ATHOR_TRELLO_API_END_NAMESPACE

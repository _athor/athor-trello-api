#include "athortrellocardmodel.h"

#include <QJsonObject>

ATHOR_TRELLO_API_BEGIN_NAMESPACE

AthorTrelloCardModel::AthorTrelloCardModel() {}

QString AthorTrelloCardModel::id() const {
    return _id;
}

void AthorTrelloCardModel::setId( const QString& id ) {
    _id = id;
}

QString AthorTrelloCardModel::name() const {
    return _name;
}

void AthorTrelloCardModel::setName( const QString& name ) {
    _name = name;
}

void AthorTrelloCardModel::fromJson( const QJsonObject& json ) {
    _id = json["id"].toString();
    _name = json["name"].toString();
}

ATHOR_TRELLO_API_END_NAMESPACE

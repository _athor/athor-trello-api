#include "athortrelloboardrequest.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonDocument>

#include "data/athortrellolistmodel.h"

ATHOR_TRELLO_API_BEGIN_NAMESPACE

QList<AthorTrelloListModel*> AthorTrelloBoardRequest::findListByIdBoard( const QString& id ) {

    QString url = _url.url();
    url = url.append( "/1/boards/" );
    url = url.append( id );
    url = url.append( "/lists" );

    _url = QUrl( url );
    QJsonArray json = AthorTrelloRequest::get().array();

    QList<AthorTrelloListModel*> responseList = {};

    for ( QJsonValue response : json ) {
        AthorTrelloListModel* athorTrelloListModel = new AthorTrelloListModel();
        athorTrelloListModel->fromJson( response.toObject() );

        responseList.append( athorTrelloListModel );
    }

    return responseList;

}

ATHOR_TRELLO_API_END_NAMESPACE
